local streams = {}
local playing = {}

function open_inputs()
	local i, card, dev
	for i, card in ipairs(OggBox.ALSA.cards()) do
		if card['input'] then
			dev = OggBox.ALSA.new(card['dev'])
			dev:start_capture()
			end
		end
	end

OggBox.http_set_handler("opencard",
	function(query)
		local out = {}
		local card = OggBox.ALSA.new(query.device)
		out['status'] = (card ~= nil)
		return out
		end)

OggBox.http_set_handler("devices",
	function(query)
		return OggBox.ALSA.cards()
		end)

OggBox.http_set_handler("play",
	function(query)
		local out = {}
		out['path'] = query.path
		local file = OggBox.AudioFile.new(query.path)
		file:play()
		playing[query.path] = file
		return out
		end)

OggBox.http_set_handler("stop",
	function(query)
		local out = {}
		out['path'] = query.path
		local file = playing[query.path]
		if file ~= nil then
			file:stop()
			playing[query.path] = nil
			end
		return out
		end)

OggBox.http_set_handler("mount",
	function(query)
		local stream
		if query.encode == "flac" then
			stream = OggBox.Stream.new_flac({
			    host="localhost",
				user=query.user,
			    password=query.password,
			    port=query.port,
			    mount=query.mount,
			    name=query.name
			    })
		else
			stream = OggBox.Stream.new_vorbis({
			    host="localhost",
				user=query.user,
			    password=query.password,
			    port=query.port,
			    mount=query.mount,
			    name=query.name,
				quality=query.quality,
				bitrate=query.bitrate
			    })
			end
		local out = {}
		if (stream ~= nil) and (stream.mounted) then
			streams[query.tag] = stream
			out['status'] = true
		else
			out['status'] = false
			end
		return out
		end)

OggBox.http_set_handler("unmount",
	function(query)
		local stream = streams[query.tag]
		local out = {}
		if stream == nil then
			out['status'] = false
		else
			streams[query.tag] = nil
			stream:unmount()
			out['status'] = true
			end
		return out
		end)

open_inputs()
OggBox.gen_noise(0.001)
