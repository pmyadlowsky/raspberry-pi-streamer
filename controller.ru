#! /usr/bin/ruby

# Web interface for RaspberryPi streaming appliance

require "rack"
require "puma"
require "json"
require "uri"
require "net/http"
require "net/ssh/gateway"
require "singleton"
require "dbus"
require "taglib"
require "pp"

Home = __dir__
DefaultsConfigFile = "#{Home}/defaults.config"
CustomConfigFile = "#{Home}/custom.config"
MediaRoot = "/media/pi"
WPAConfig = "/etc/wpa_supplicant/wpa_supplicant.conf"

MediaTypes = {
	"mp3" => "audio",
	"m4a" => "audio",
	"ogg" => "audio",
	"wav" => "audio",
	"wma" => "audio",
	"flac" => "audio",
	"jpg" => "img",
	"jpeg" => "img",
	"gif" => "img",
	}

class SSHGate
	attr_reader :error, :host, :local_port
	def initialize(app, port, host, user, key)
		@app = app
		@host = host
		@user = user
		@remote_port = port
		@keyfile = key
		@gate = nil
		@local_port = nil
		begin
"OPEN GATE #{@host}, #{@user}, #{@keyfile}"
			@gate = Net::SSH::Gateway.new(@host, @user, keys: [@keyfile])
			@local_port = @gate.open("localhost", @remote_port)
		rescue Exception => exc
puts "SSH GATE FAILED #{exc.message}"
			@app.log "ssh gate failed: #{exc.message}"
			@local_port = nil
			end
		end
	def close
		return unless @gate
		@gate.shutdown!
		@gate = nil
		@local_port = nil
		end
	end

class Icecast
	attr_reader :port
	KeepAliveInterval = 60
	StatusInterval = 10
	def initialize(app, config)
		@app = app
		@port = config[:port]
		@config = config
		@ssh_gate = nil
		@remote_host = config[:host] || "localhost"
		@opened_port = nil
		@status_mutex = Mutex.new
		@status_cache = nil
		if config[:key]
			@ssh_gate = SSHGate.new(@app, @port, config[:host],
				config[:user], config[:key])
			keep_alive
			end
		start_status_thread
		end
	def url
		"http://#{@remote_host}:#{@port}"
		end
	def keep_alive
		return unless @ssh_gate && @ssh_gate.local_port
		Thread.start do
			while true
				query("/server_version.xsl")
				sleep(KeepAliveInterval)
				end
			end
		end
	def close_gate
		@ssh_gate.close if @ssh_gate
		end
	def cleanup
		close_gate
		end
	def remote_host
		return @ssh_gate.host if @ssh_gate
		@remote_host
		end
	def local_port
		 (@ssh_gate && @ssh_gate.local_port) || @port
		end
	def query(path, json = nil)
		uri = URI("http://localhost:#{local_port}#{path}")
		out = nil
		begin
			resp = Net::HTTP::get(uri)
			out = (json ? JSON::parse(resp) : resp)
		rescue Exception => exc
			nil
			end
		out
		end
	def get_status
		begin
			query("/status-json.xsl", :json)
		rescue Exception => exc
			nil
			end
		end
	def start_status_thread
		Thread.start do
			while true
				cache = get_status
				@status_mutex.lock
				@status_cache = cache
				@status_mutex.unlock
				sleep(StatusInterval)
				end
			end
		end
	def status(force = nil)
		@status_mutex.lock
		cache = (force ? get_status : @status_cache)
		@status_mutex.unlock
		cache
		end
	def sources
		stat = status
		return [] unless stat.is_a?(Hash)
		return [] unless stat['icestats']
		sources = stat['icestats']['source']
		return [] unless sources
		if sources.is_a?(Array)
			return sources
		else
			return [sources]
			end
		end
	def source_present(stream)
		return false unless stream
		sources.each do |src|
			if src['listenurl'].to_s =~ /\/#{stream}$/
				return true
				end
			end
		false
		end
	end

class Wicd
	include Singleton
	StatusNotConnected = 0;
	StatusConnecting = 1;
	StatusConnectedWireless = 2;
	StatusConnectedWired = 3;
	def initialize
		@dbus = DBus::SystemBus.instance
		@service = @dbus.service("org.wicd.daemon")
		@main = @service.object("/org/wicd/daemon")
		@main.introspect
		@wired = @service.object("/org/wicd/daemon/wired")
		@wired.introspect
		@wireless = @service.object("/org/wicd/daemon/wireless")
		@wireless.introspect
		@wifi_nets = []
		end
	def wired_available
		@wired.IsWiredUp[0] && @wired.CheckPluggedIn[0]
		end
	def status
		status = @main.GetConnectionStatus[0]
		detail = {
			:state => "unknown",
			:connected => false,
			:interface => @main.GetCurrentInterface[0]
			}
		if (status[0] == StatusConnectedWired) ||
				(status[0] == StatusConnectedWireless)
			detail[:connected] = true
			if status[0] == StatusConnectedWired
				detail[:state] = "wired"
				detail[:type] = :wired
				detail[:ip_addr] = status[1][0]
				detail[:network] = "wired-default"
			else
				detail[:state] = "wireless"
				detail[:type] = :wireless
				detail[:ip_addr] = status[1][0]
				detail[:essid] = status[1][1]
				detail[:strength] = status[1][2].to_i
				detail[:networkid] = status[1][3].to_i
				detail[:network] = detail[:essid]
				end
		elsif status[0] == StatusNotConnected
			detail[:state] = "disconnected"
		elsif status[0] == StatusConnecting
			detail[:state] = "connecting"
			end
		detail
		end
	def default_wired
		@wired.GetDefaultWiredNetwork[0]
		end
	def wired_networks
		if @wired.IsWiredUp
			rec = {}
			rec[:type] = :wired
			rec[:nid] = 0
			rec[:ssid] = default_wired
			[rec]
		else
			[]
			end
		end
	def wireless_available
		@wireless.IsWirelessUp
		end
	def scan_wireless_networks
		return unless wireless_available
		@wireless.Scan(true)
		netcount = @wireless.GetNumberOfNetworks[0].to_i
		nets = []
		netcount.times do |nid|
			nets << (wifi = {})
			wifi[:nid] = nid
			wifi[:type] = :wireless
			wifi[:ssid] = @wireless.GetWirelessProperty(nid, 'essid')[0]
			wifi[:bssid] = @wireless.GetWirelessProperty(nid, 'bssid')[0]
			wifi[:channel] =
				@wireless.GetWirelessProperty(nid, 'channel')[0].to_i
			wifi[:encrypted] =
				@wireless.GetWirelessProperty(nid, 'encryption')[0]
			wifi[:encryption] =
				@wireless.GetWirelessProperty(nid, 'encryption_method')[0]
			wifi[:strength] =
				@wireless.GetWirelessProperty(nid, 'quality')[0].to_i
			wifi[:mode] = @wireless.GetWirelessProperty(nid, 'mode')[0]
			end
		@wifi_nets = nets
		end
	def wireless_networks
		return [] unless @wireless.IsWirelessUp
		return @wifi_nets
		end
	def connect_wired
#		return false unless wired_available
		@wired.ConnectWired
		return true
		end
	def connect_wireless(ssid, password)
		return false unless @wireless.IsWirelessUp
		wireless_networks.each do |net|
			next unless net[:ssid] == ssid
			@wireless.SetWirelessProperty(net[:nid], "apsk", password)
			@wireless.SetWirelessProperty(net[:nid], "key", password)
			@wireless.SetWirelessProperty(net[:nid], "automatic", "1")
			@wireless.SetWirelessProperty(net[:nid], "dns1", "None")
			@wireless.SetWirelessProperty(net[:nid], "dns2", "None")
			@wireless.SetWirelessProperty(net[:nid], "dns3", "None")
			@wireless.SetWirelessProperty(net[:nid], "dns_domain", "None")
			@wireless.SetWirelessProperty(net[:nid], "enctype", "wpa")
			@wireless.SetWirelessProperty(net[:nid], "gateway", "None")
			@wireless.SetWirelessProperty(net[:nid], "ip", "None")
			@wireless.SetWirelessProperty(net[:nid], "netmask", "None")
			@wireless.SetWirelessProperty(net[:nid], "search_domain", "None")
			@wireless.ConnectWireless(net[:nid])
			return true
			end
		false
		end
	end

class Network
	include Singleton
	def initialize
		@networks = {}
		end
	def app=(app)
		@app = app
		end
	def wired_available
		Wicd.instance.wired_available
		end
	def wireless_available
		Wicd.instance.wireless_available
		end
	def [](ssid)
		@networks[ssid].dup
		end
	def status
		Wicd.instance.status
		end
	def scan_wifi
		nets = {}
		Wicd.instance.scan_wireless_networks
		Wicd.instance.wireless_networks.each do |net|
			nets[net[:ssid]] = net
			end
		Wicd.instance.wired_networks.each do |net|
			nets[net[:ssid]] = net
			end
		@networks = nets
		end
	def default_wired
		Wicd.instance.default_wired
		end
	def wired_nets
		cache = {}
		@networks.each do |key, rec|
			cache[key] = rec if rec[:type] == :wired
			end
		cache
		end
	def wireless_nets
		cache = {}
		@networks.each do |key, rec|
			cache[key] = rec if rec[:type] == :wireless
			end
		cache
		end
	def connect_wireless(ssid, password)
		Wicd.instance.connect_wireless(ssid, password)
		end
	def connect_wired
		Wicd.instance.connect_wired
		end
	def switch_net(ssid, password, logger = nil)
		@net = @networks[ssid]
		return false unless @net
		stat = status
		if stat[:connected] && (ssid == stat[:network])
			@app.log "already connected to #{stat[:network]}"
			return false
			end
		if @net[:type] == :wired
			logger.call("CONNECTING TO WIRED") if logger
			connect_wired
		else
			logger.call("CONNECTING TO WIFI #{@net[:nid]}") if logger
			connect_wireless(ssid, password)
			end
		true
		end
	end

class PersistentHash
	def initialize(fixed_config_path, custom_config_path)
		@custom_path = custom_config_path
		@custom = {}
		@fixed = load_config(fixed_config_path)
		@custom = load_config(@custom_path)
		end
	def load_config(src_path)
		config = {}
		begin
			File.open(src_path, File::CREAT|File::RDONLY) do |file|
				config = JSON::parse(file.read)
				end
		rescue Exception => exc
			end
		config
		end
	def [](key)
		@custom[key] || @fixed[key]
		end
	def save_custom(table, path)
		begin
			File::open(path, File::TRUNC|File::WRONLY) do |file|
				file.write(table.to_json)
				end
		rescue Exception => exc
			end
		end
	def []=(key, value)
		@custom[key] = value
		save_custom(@custom, @custom_path)
		end
	end

class ScrollingLog
	# self-clearing log cache
	def initialize
		@log = []
		@mutex = Mutex.new
		end
	def add(entry)
		@mutex.lock
		@log << entry
		@mutex.unlock
		@log
		end
	def dump(start = 0)
		@mutex.lock
		lines = (start...@log.size).map{ |i| @log[i] }
		@mutex.unlock
		lines
		end
	end

class FileCache
	def initialize(srcpath)
		@srcpath = srcpath
		@cache = nil
		@stamp = nil
		end
	def get
		if @cache.nil? || (File::mtime(@srcpath) > @stamp)
			@stamp = Time.now
			@cache = File.open(@srcpath) {|f| f.read}
			end
		@cache
		end
	end

class OggBox
	include Singleton
	def initialize
		@log = ScrollingLog.new
		@port = 8080
		end
	def app=(app)
		@app = app
		end
	def port=(port)
		@port = port
		end
	def log(msg)
		@log.add(msg)
		end
	def dump_log(start = 0)
		@log.dump(start)
		end
	def start(script)
		return if up
		system("/usr/bin/screen -d -m -S oggbox /usr/local/bin/oggbox -l \"http://localhost/oblog\" #{script}")
		end
	def stop(tries = 1)
		return true unless up
		tries.times do |i|
			query("/exit")
			sleep(1)
			return true unless up
			end
		log("OggBox shutdown failed")
		false
		end
	def map_quality(quality)
		return 96 if quality <= 0.2
		return 128 if quality <= 0.4
		return 192 if quality <= 0.6
		return 256 if quality <= 0.8
		return 320 if quality <= 0.9
		500
		end
	def query(path, json = nil)
		uri = URI("http://localhost:#{@port}#{path}")
		begin
			resp = Net::HTTP::get(uri)
			json ? JSON::parse(resp) : resp
		rescue Exception => exc
puts "OGGBOX QUERY #{path}, #{exc.message}" unless exc.message =~ /Failed to open/
			nil
			end
		end
	def mount_stream(tag, user, password, port, mount_pts,
			name, quality, encode)
		bitrate = map_quality(quality)
		mount_pts.each do |mount_pt|
			params = {
				"tag" => tag,
				"user" => user,
				"password" => password.to_s,
				"port" => port.to_s,
				"mount" => mount_pt.to_s,
				"name" => name.to_s,
				"quality" => quality.to_s,
				"bitrate" => bitrate.to_s,
				"encode" => encode
				}
			qs = params.keys.map do |key|
				"#{key}=#{URI::encode(params[key])}"
				end.join("&")
			resp = query("/mount?#{qs}", :json)
			if resp['status']
				@app.log "mount #{mount_pt}: #{resp.inspect}" if @app
				return mount_pt
				end
			end
		return nil
		end
	def unmount_stream(tag)
		query("/unmount?tag=#{tag}", :json)
		end
	def up(tries = 1)
		# is oggbox up?
		tries.times do |i|
			sleep(1) unless i == 0
			return true if query("/ping")
			end
		return false
		end
	end

class Controller
	ConnectionPatience = 60
	def initialize
		@app_log = ScrollingLog.new
		@log = File.new("/tmp/controller.log", "w")
		@responders = {}
		@mount_pts = {}
		@page_html = FileCache.new("#{Home}/page.html")
		@home = __dir__
		map_responders
		@config =
			PersistentHash.new(DefaultsConfigFile, CustomConfigFile)
		log("START!")
		Network.instance.app = self
		Network.instance.scan_wifi
#		first_connect
		log "STATUS #{Network.instance.status.inspect}"
		OggBox.instance.app = self
		OggBox.instance.port = @config['oggbox-http-port']
		hostname = `/usr/bin/hostname`.strip
		hostname << ".local" unless hostname =~ /\.local$/
		@local_icecast =
			Icecast.new(self,
				{	:host => hostname,
					:port => @config['local-icecast-port']
					})
		@remote_icecast_config = nil
		await_network
		end
	def await_network
		count = 0
		Thread.start do
			while true
				@remote_icecast_config = remote_config
				if @remote_icecast_config
					@remote_icecast = Icecast.new(self,
						{	:host => @remote_icecast_config['remote-icecast-host'],
							:user => @config['remote-ssh-user'],
							:key => @config['remote-ssh-key'],
							:port => @remote_icecast_config['remote-icecast-port']
							})
					log "network connection established"
					break
					end
				if count == 8
					log "bring up hotspot"
					hotspot(:up) if count == 8
					end
				sleep(5)
				count += 1
				end
			end
		end
	def hotspot(action = :up)
		# wifi hotspot, for configuration via ad hoc network
		if action == :up
			system("sudo #{@home}/hotspot_up")
		else
			system("sudo #{@home}/hotspot_down")
			end
		end
	def remote_config
		begin
		Net::SSH.start(
				@config['remote-ssh-host'],
				@config['remote-ssh-user'],
				keys: [@config['remote-ssh-key']]) do |ssh|
			stuff = ssh.exec!("cat /home/pmy/rpi_config.json").to_s.strip
			JSON::parse(stuff)
			end
		rescue Exception => exc
			log "REMOTE CONFIG FAILED: #{exc.message}"
			nil
			end
		end
	def stamp
		Time.now.strftime("%H:%M:%S")
		end
	def wifi_creds(credsfile)
		state = :orig
		networks = {}
		ssid = key = nil
		avail_ssids = Network.instance.wireless_nets.keys
		return networks if avail_ssids.empty?
		IO::readlines(credsfile).each do |line|
			if state == :orig
				if line =~ /^\s*network\s*=\s*{/
					state = :net
					end
			elsif state == :net
				if line =~ /^\s*ssid=(.+)/
					ssid = $1.gsub('"', "")
				elsif line =~ /^\s*psk=(.+)/
					key = $1.gsub('"', "")
				elsif line =~ /^\s*}/
					state = :orig
					if ssid && key && avail_ssids.include?(ssid)
						networks[ssid] = key
						end
					ssid = key = nil
					end
				end
			end
		return nil if networks.empty?
		ssid = networks.keys[0]
		{:ssid => ssid, :password => networks[ssid]}
		end
	def connect_wired
		return nil unless Network.instance.wired_available
		log "first: connect wired..."
		3.times do |j|
			log "CONNECT WIRED #{Network.instance.connect_wired.inspect}"
			20.times do |i|
				sleep(1)
				status = Network.instance.status
				log "WIRED STATUS #{status.inspect}"
				log "AVAILABLE #{Network.instance.wired_available.inspect}"
				break if status[:state] == "disconnected"
				return status if status[:connected]
				end
			log "HIT WIRED AGAIN"
			end
		nil
		end
	def connect_wireless(ssid, password)
		return nil unless Network.instance.wireless_available
		log "first: connect wireless '#{ssid}'..."
		Network.instance.connect_wireless(ssid, password)
		ConnectionPatience.times do |i|
			sleep(1)
			status = Network.instance.status
			return status if status[:connected]
			end
		nil
		end
	def first_connect
		10.times do # wait for network interface to wake up
			sleep(1)
			break if Network.instance.wired_available
			log "eth0 wait..."
			end
		status = Network.instance.status
		if status[:connected]
			log "first: already connected"
			return status
			end
		# try wired connection first
		state = connect_wired
		return state if state
		# try wireless connection if wifi profile exists
		creds = wifi_creds(WPAConfig)
		return status unless creds
		state = connect_wireless(creds[:ssid], creds[:password])
		return state || status
		end
	def log(msg)
		stamp = Time.now.strftime("%Y-%m-%d %H:%M:%S")
		@app_log.add("#{stamp} #{msg}")
		@log.puts "#{stamp} #{msg}"
		@log.flush
		end
	def play_file(path)
		begin
			query = "path=#{URI::escape(path)}"
			resp = OggBox.instance.query("/play?#{query}", :json)
			puts "PLAY #{resp.inspect}"
			resp
		rescue Exception => exc
			puts "PLAY #{exc.message}"
			{}
			end
		end
	def stop_file(path)
		begin
			query = "path=#{URI::escape(path)}"
			resp = OggBox.instance.query("/stop?#{query}", :json)
			puts "STOP #{resp.inspect}"
			resp
		rescue Exception => exc
			puts "STOP #{exc.message}"
			{}
			end
		end
	def json_response(object)
		[200, {"content-type" => "application/json"},
			[object.to_json]]
		end
	def html_response(html)
		[200, {"content-type" => "text/html"}, [html]]
		end
	def icecast_streams(server)
		sources = server.sources
		return "No streams present" if sources.size < 1
		out = "<table>"
		out << "<tr><th>Stream</th><th>Channels</th><th>Sampling</th>"
		out << "<th>Bitrate</th><th>Type</th><th>Player</th>"
		out << "<th><abbr title=\"open in external player\">M3U</abbr></th>"
		out << "</tr>\n"
		domain = "#{server.remote_host}:#{server.port}"
		sources.each do |source|
			out << "<tr>"
			source['listenurl'] =~ /([^\/]+)$/
			stream = $1
			out << "<td class=\"tcell\">#{stream}</td>"
			out << "<td class=\"tcellr\">#{source['channels']}</td>"
			out << "<td class=\"tcellr\">#{source['samplerate']}</td>"
			out << "<td class=\"tcellr\">#{source['bitrate']}</td>"
			out << "<td class=\"tcell\">#{source['server_type']}</td>"
			out << "<td class=\"tcell\">"
			out << "<audio controls=\"controls\" preload=\"none\">"
			out << "<source src=\"http://#{domain}/#{stream}\""
			out << " type=\"#{source['server_type']}\">"
			out << "</source></audio></td>"
			out << "<td>"
			m3u = "#{stream}.m3u"
			out << "<a href=\"http://#{domain}/#{m3u}\">#{m3u}</a>"
			out << "</td>"
			out << "</tr>\n"
			end
		out << "</table>"
		out
		end
	def main_page
		[200, {"Content-Type" => "text/html"}, [@page_html.get]]
		end
	def connection_link(ssid, type, pwid, connected)
		return ssid if connected
		html = ""
		html << "<a href=\"#\" title=\"connect to this network\""
		html << " onclick=\"return prep_netchange('#{ssid}','#{type.to_s}','#{pwid}')\""
		html << ">#{ssid}</a>"
		html
		end
	def ssid_match(ssid, status_ssid)
		return true if ssid == status_ssid
		return (ssid =~ /wired/) && (status_ssid == "wired")
		end
	def networks_display
		ssids = []
		nets = Network.instance.wired_nets
		ssids.concat(nets.keys.sort)
		nets = Network.instance.wireless_nets
		ssids.concat(nets.keys.sort)
		if ssids.size > 0
			status = Network.instance.status
			html = "<table>"
			html << "<tr><th>ESSID</th><th>Channel</th>"
			html << "<th>BSSID</th><th>Encr</th><th>Qual</th>"
			html << "<th>Addr</th></tr>"
			msg = "No connection"
			ssids.each do |ssid|
				net = Network.instance[ssid]
				next unless net
				if ssid_match(ssid, status[:network])
					ip_addr = status[:ip_addr]
					msg = "Connected to #{status[:network]} at #{ip_addr}"
				else
					ip_addr = ""
					end
				html << "<tr>"
				html << "<td class=\"tcell\">"
				pwid = "pw-#{net[:type]}-#{net[:nid]}"
				html << connection_link(ssid, net[:type], pwid, ip_addr != "")
				html << "</td><td class=\"tcellr\">#{net[:channel]}</td>"
				html << "<td class=\"tcell\">#{net[:bssid]}</td>"
				html << "<td class=\"tcell\">#{net[:encryption]}</td>"
				html << "<td class=\"tcellr\">#{net[:strength]}</td>"
				html << "<td class=\"tcellr\">#{ip_addr}</td>"
				html << "</tr>"
				html << "<tr id=\"#{pwid}\" class=\"pwrow\">"
				html << "<td class=\"tcell\" colspan=\"6\">"
				html << "password: <input id=\"text-#{pwid}\" type=\"text\"/>"
				html << " <input class=\"conbtn\" id=\"con-#{pwid}\" type=\"button\" value=\"Connect\"/>"
				html << " <input class=\"conbtn\" id=\"can-#{pwid}\" type=\"button\" value=\"Cancel\"/>"
				html << "</td>"
				html << "</tr>\n"
				end
			html << "</table>"
		else
			html = "None"
			end
		json_response({
			"chart" => html,
			"msg" => msg
			})
		end
	def device_display
		devs = OggBox.instance.query("/devices", :json) || []
		html = ""
		if devs.empty?
			html << "None (is OggBox running?)"
		else
			html << "<table>"
			html << "<tr><th></th><th>Name</th><th>Device</th>"
			html << "<th>Dir</th></tr>"
			devs.each do |dev|
				html << "<tr>"
				html << "<td>"
				if dev['open']
					html << "<img title=\"device opened\" src=\"/images/greenlite-16.png\"/>"
					end
				html << "</td>"
				html << "<td class=\"tcell\">#{dev['name']}</td>"
				html << "<td class=\"tcell\">#{dev['dev']}</td>"
				dirs = []
				dirs << "in" if dev['input']
				dirs << "out" if dev['output']
				html << "<td class=\"tcell\">#{dirs.join("/")}</td>"
				html << "</tr>"
				end
			html << "</table>"
			end
		json_response({"html" => html})
		end
	def connect_icecast(channel, mount_pts, icecast, encoding, quality)
		return { "status" => false } unless icecast
		port = icecast.local_port
		if channel == "remote"
			user = @remote_icecast_config['remote-icecast-user']
			password = @remote_icecast_config['remote-icecast-password']
			name = "Home Stream"
		else # local
			user = "source"
			password = @config['local-icecast-password']
			name = "Local Stream"
			end
		mount_pt = OggBox.instance.mount_stream(channel, user, password,
			port, mount_pts, name, quality, encoding)
		return {"status" => false} unless mount_pt
		{
			"status" => "#{channel}_connecting",
			"mount_pt" => mount_pt
			}
		end
	def disconnect_icecast(channel)
		OggBox.instance.unmount_stream(channel)
		{"status" => "#{channel}_disconnecting"}
		end
	def audio_metadata(path)
		fullpath = "#{MediaRoot}/#{path}"
		data = {}
		TagLib::FileRef.open(fullpath) do |file|
			tag = file.tag
			if tag
				data[:path] = path
				data[:size] = File::size(fullpath)
				data[:title] = tag.title
				data[:artist] = tag.artist
				data[:album] = tag.album
				if tag.year.to_i > 0
					data[:year] = tag.year
					end
				data[:genre] = tag.genre
				data[:comment] = tag.comment
				end
			props = file.audio_properties
			if props
				data[:sample_rate] = props.sample_rate
				data[:channels] = props.channels
				data[:duration] = props.length_in_seconds
				data[:bitrate] = props.bitrate
				end
			end
		data
		end
	def metarow(label, data)
		stripped = data.to_s.strip
		return "" if stripped.empty?
		out = ""
		out << "<tr class=\"metadata-row\">"
		out << "<td class=\"label\">#{label}:</td><td>#{stripped}</td>"
		out << "</tr>"
		out
		end
	def hhmmss(sec)
		hours = sec / 3600
		min = (sec % 3600) / 60
		sec = (sec % 3600) % 60
		if hours == 0
			"%d:%02d" % [min, sec]
		else
			"%d:%02d:%02d" % [hours, min, sec]
			end
		end
	def filesize(arg)
		if arg.is_a?(String)
			begin
				size = File::size(arg)
			rescue Exception => exc
				return nil
				end
		else
			size = arg.to_i
			end
		if size >= 1000000000
			return "%3.1fG" % (size / 1000000000.0)
		elsif size >= 1000000
			return "%3.1fM" % (size / 1000000.0)
		elsif size >= 1000
			return "%3.1fK" % (size / 1000.0)
		else
			return size.to_s
			end
		end
	def audio_metadata_chart(path)
		data = audio_metadata(path)
		out = "<table>"
		ok = false
		if data[:path]
			ok = true
			out << metarow("file path", data[:path])
			out << metarow("size", filesize(data[:size]))
			out << metarow("title", data[:title])
			out << metarow("artist", data[:artist])
			out << metarow("album", data[:album])
			if data[:year]
				out << metarow("year", data[:year])
				end
			out << metarow("genre", data[:genre])
			out << metarow("comment", data[:comment])
			end
		if data[:sample_rate]
			ok = true
			rate_info = data[:sample_rate].to_s + "Hz, "
			rate_info << (data[:channels] < 2 ? "mono" : "stereo")
			out << metarow("sampling", rate_info)
			out << metarow("duration", hhmmss(data[:duration]))
			out << metarow("bitrate", data[:bitrate])
			end
		unless ok
			out << "<tr><td colspan=\"2\">this might not be an audio file</td></tr>"
			end
		out << "</table>"
		out
		end
	def lsdir(dir)
		path = "#{MediaRoot}/#{dir}"
		out = {}
		out[:dirs] = []
		out[:files] = []
		here = Dir::pwd
		begin
			Dir::chdir(path)
		rescue Exception => exc
			out[:files] << "### no access ###"
			return out
			end
		Dir::entries(".").sort_by{|entry| entry.downcase}.each do |entry|
			next if (entry == ".") || (entry == "..")
			next if entry =~ /^\./
			if File::directory?(entry)
				out[:dirs] << entry
			elsif File::file?(entry)
				out[:files] << entry
				end
			end
		Dir::chdir(here)
		out
		end
	def directory_item(dir, entry)
		"<li class=\"directory collapsed\" rel=\"#{dir}#{entry}/\">" +
        "<a href=\"#\" rel=\"#{dir}#{entry}/\">#{entry}</a></li>"
		end
	def file_item(dir, entry)
		ext = File::extname(entry)[1..-1]
		"<li class=\"file ext_#{ext}\" rel=\"#{dir}#{entry}\">" +
			"<a href=\"#\" rel=\"#{dir}#{entry}\">#{entry}</a></li>"
		end
	def stream_ready
		return false unless OggBox.instance.up
		if @local_icecast && @local_icecast.source_present(
							@config['local-icecast-mount-point'])
			return true
			end
		!@remote_icecast.nil? &&
			@remote_icecast.source_present(@mount_pts['remote'])
		end
	def map_responders
		@responders[''] = lambda { |req| main_page }
		@responders['playmedia'] = lambda do |req|
			json_response(play_file(req.params['path']))
			end
		@responders['stopmedia'] = lambda do |req|
			json_response(stop_file(req.params['path']))
			end
		@responders['obstop'] = lambda do |req|
			disconnect_icecast("local")
			disconnect_icecast("remote")
			state = OggBox.instance.stop(3)
			json_response({"status" => state})
			end
		@responders['obstart'] = lambda do |req|
			OggBox.instance.start(@config['oggbox-script'])
			json_response({"status" => true})
			end
		@responders['obup'] = lambda do |req|
			json_response({"status" => OggBox.instance.up})
			end
		@responders['ping'] = lambda do |req|
			if @config['oggbox-autostart'] == "true"
				OggBox.instance.start(@config['oggbox-script'])
				end
			json_response({"status" => "pong"})
			end
		@responders['icready'] = lambda do |req|
			resp = {}
			resp['oggbox_up'] = OggBox.instance.up
			resp['local_status'] = !@local_icecast.status.nil?
			resp['local_connect'] =
				resp['local_status'] &&
				@local_icecast.source_present(
					@config['local-icecast-mount-point'])
			resp['remote_status'] =
				(@remote_icecast ? !@remote_icecast.status.nil? : false)
			remote_mount_point = @mount_pts['remote']
			resp['remote_connect'] =
				resp['remote_status'] &&
				(@remote_icecast ? 
					@remote_icecast.source_present(
						remote_mount_point) : false)
			if resp['local_connect']
				resp['local_url'] = "#{@local_icecast.url}/" +
					@config['local-icecast-mount-point']
				end
			if resp['remote_connect']
				resp['remote_url'] = "#{@remote_icecast.url}/" +
					remote_mount_point
				end
			resp['remote_host'] =
				@remote_icecast_config ? 
					@remote_icecast_config['remote-icecast-host'].to_s +
					":" +
					@remote_icecast_config['remote-icecast-port'].to_s :
					"(unreachable)"
			json_response(resp)
			end
		@responders['icview'] = lambda do |req|
			if req.params['s'].to_s == "remote"
				server = @remote_icecast
			else
				server = @local_icecast
				end
			json_response({
				"status" => (server.status ? "Running" : "Not Running"),
				"streams" => icecast_streams(server)
				})
			end
		@responders['netstat'] = lambda do |req|
			status = Network.instance.status
			json_response({
				"connected" => status[:connected],
				"state" => status[:state]
				})
			end
		@responders['wifinets'] = lambda { |req| networks_display }
		@responders['shutdown'] = lambda do |req|
			system("sudo /sbin/shutdown -fh now")
			end
		@responders['reboot'] = lambda do |req|
			system("sudo /sbin/reboot")
			end
		@responders['devices'] = lambda { |req| device_display }
		@responders['oblog'] = lambda do |req|
			OggBox.instance.log(req.params['msg'])
			json_response({"status" => true})
			end
		@responders['oblogview'] = lambda do |req|
			start = (req.params['s'] || 0).to_i
			json_response({"log" => OggBox.instance.dump_log(start)})
			end
		@responders['applogview'] = lambda do |req|
			start = (req.params['s'] || 0).to_i
			json_response({"log" => @app_log.dump(start)})
			end
		@responders['preload'] = lambda do |req|
			config = {}
			["oggbox-autostart", "start-tab",
				"local-icecast-encoding", "local-icecast-bitrate",
				"remote-icecast-encoding", "remote-icecast-bitrate",
				].each do |key|
				ukey = key.gsub("-", "_")
				config[ukey] = @config[key].to_s
				end
			json_response(config)
			end
		@responders['setconfig'] = lambda do |req|
			@config[req.params['key']] = req.params['val']
			json_response({'status' => true})
			end
		@responders['changenet'] = lambda do |req|
			ssid = req.params['ssid'].to_s
			secret = req.params['secret'].to_s.strip
			net = Network.instance[ssid]
			res = Network.instance.switch_net(ssid, secret,
				lambda {|msg| log(msg)})
			json_response({
				'msg' => "Connecting to #{ssid}...",
				'change' => res
				})
			end
		@responders['settab'] = lambda do |req|
			@config['start-tab'] = req.params['tab'].to_s
			json_response({'status' => true})
			end
		@responders['wifiscan'] = lambda do |req|
			Network.instance.scan_wifi
			json_response({'status' => true})
			end
		@responders['minfo'] = lambda do |req|
			# file metadata
			out = {}
			path = JSON::parse(req['path'].to_s)[0]
			if path =~ /\.(\w+)$/
				suffix = $1.downcase
			else
				suffix = "unknown"
				end
			out['media'] = MediaTypes[suffix] || "unknown"
			if out['media'] == "audio"
				out['metadata'] = audio_metadata_chart(path)
				out['download_url'] = "/media#{path}"
				out['filepath'] = "#{MediaRoot}#{path}"
				out['streamready'] = stream_ready
				end
			json_response(out)
			end
		@responders['icecast'] = lambda do |req|
			channel = req.params['ch'].to_s
			if channel == "remote"
				icecast = @remote_icecast
				mount_pts =
					(@remote_icecast_config || {})['mount-points'] || []
			else
				channel = "local"
				icecast = @local_icecast
				mount_pts = [@config['local-icecast-mount-point']]
				end
			if !OggBox.instance.up
				resp = {'status' => false}
			elsif !icecast
				resp = {'status' => false}
			elsif icecast.source_present(@mount_pts[channel])
				resp = disconnect_icecast(channel)
			else
				encoding = @config[channel + "-icecast-encoding"]
				encoding = "vorbis" unless encoding == "flac"
				quality = @config[channel + "-icecast-bitrate"].to_f
				quality = 0.8 if quality == 0
				resp = connect_icecast(channel, mount_pts,
					icecast, encoding, quality)
				@mount_pts[channel] = resp['mount_pt']
				end
			json_response(resp)
			end
		@responders['ls'] = lambda do |req| # file system
			dir = URI::decode(req['dir'].to_s)
			html = ""
			html << "<ul class=\"jqueryFileTree\" style=\"display: none\">"
			catalog = lsdir(dir)
			catalog[:dirs].each do |entry|
				html << directory_item(dir, entry)
				end
			catalog[:files].each do |entry|
				html << file_item(dir, entry)
				end
			html << "</ul>"
			html_response(html)
			end
		@responders['vols'] = lambda do |req| # storage volumes
			vols = []
			Dir::entries(MediaRoot).each do |entry|
				next if entry =~ /^\./
				vols << entry
				end
			json_response({"vols" => vols})
			end
		@responders.default = lambda do |req|
			[200, {"Content-Type" => "text/plain"}, ["what?"]]
			end
		end
	def call(env)
		req = Rack::Request.new(env)
		path = env['PATH_INFO'].sub(/^\//, "")
		@responders[path].call(req)
		end
	def cleanup
		log "cleaning up..."
		@local_icecast.cleanup if @local_icecast
		@remote_icecast.cleanup if @remote_icecast
		log "...done"
		@log.close if @log
		end
	end

controller = Controller.new

at_exit do
	controller.cleanup
	end

run controller
